﻿using System;
using System.Reflection;
using System.Text.Json;
using System.Diagnostics;
using System.Text.Json;

namespace HomeWorks.Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            
            DateTime dt = new DateTime();
            dt = DateTime.Now;
            var sample = new F().Get();
            var str = string.Empty;
            #region Custom serialization   
            str = CustomSerializer.Serialize(sample.Get());
            var ts = new TimeSpan(DateTime.Now.Ticks - dt.Ticks);
            Console.WriteLine("Serialized string = "+ str);
            Console.WriteLine($"Custom Serialize Time  = {ts.TotalMilliseconds} ms");

            var secondsample = new F();
            dt = DateTime.Now;
            CustomSerializer.Deserialize(secondsample, str);
            ts = new TimeSpan(DateTime.Now.Ticks - dt.Ticks);
            Console.WriteLine($"Custom Deserialize Time  = {ts.TotalMilliseconds} ms");
            #endregion
            #region JSON serialization 
            var options = new JsonSerializerOptions()
            {
                IncludeFields = true,
                PropertyNameCaseInsensitive = true
            };

            dt = DateTime.Now;
            var jsonstring =  JsonSerializer.Serialize(sample,sample.GetType(), options);
            ts = new TimeSpan(DateTime.Now.Ticks - dt.Ticks);
            Console.WriteLine($"Serialized JSONstring =  { jsonstring}");
            Console.WriteLine($"JSON Serialize Time  = {ts.TotalMilliseconds} ms");
            dt = DateTime.Now;
            JsonSerializer.Deserialize(jsonstring, typeof(F), options);
            ts = new TimeSpan(DateTime.Now.Ticks - dt.Ticks);
            Console.WriteLine($"JSON Deserialize Time  = {ts.TotalMilliseconds} ms");
            #endregion

        }

        class F
        {
            public int i1, i2, i3, i4, i5;
            public F Get() => new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
        }
    }
}
