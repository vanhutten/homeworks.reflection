﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace HomeWorks.Reflection
{
    public static class CustomSerializer
    {
        static BindingFlags BindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
        public static string Serialize(object obj)
        {
            var type = obj.GetType();

            var fields = type.GetTypeInfo().GetRuntimeFields();
            var props = type.GetTypeInfo().GetRuntimeProperties();
            var sb = new StringBuilder();
            foreach (var field in fields)
            {
                sb.AppendLine($"{field.Name},{field.GetValue(obj).ToString()}");
            }
            return sb.ToString();
        }


        public static void Deserialize(object obj, string str)
        {
            var strings = str.Split('\n');
            foreach(var str2 in strings)
            {
                var splittedfield = str2.Split(',');
                var field = obj.GetType().GetField(splittedfield[0], BindingFlags);
                if (field != null)
                {
                    field.SetValue(obj, Convert.ChangeType(splittedfield[1], field.FieldType));
                }

            }

        }
    }
}
